# Fraktale

## Team: 
Meister Nelia, Fidyaeva Margarita

## Funktionsbeschreibung
- Ein  unendlich oft wiederholter  rekursiver  Erzeugungsprozess 

## Bezug zu Mathematik
- Zeichnen komplexe geometrische Figuren oder Muster mithilfe  mathematischer Formeln

## Zeitplan
- Grobkonzept Ende Okt
- Feinkonzept Mitte Nov., Klärung benötigte Technologien
- Bis 2. Woche Dez.: Programmierung Grundgerüst 
- Prototyp für Test Ende Dez.

## Aufgabenteilung
- Alle Aufgaben werden gemeinsam bearbeitet

## Zusammenarbeit
- Regelmäßige Treffen 14-tägig in Mensa
- Informationsaustausch

## Dokumentation
- Basierend auf Javadoc
- Sprache Deutsch

## Offene Punkte, Fragen
- (?)
